﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using JInvoice_core.Models;

namespace JInvoice_core.Controllers
{
    public class InvoiceListsController : Controller
    {
        private readonly Invoice_ProductContext _context;

        public InvoiceListsController(Invoice_ProductContext context)
        {
            _context = context;
        }

        // GET: InvoiceLists
        public async Task<IActionResult> Index()
        {
            return View(await _context.InvoiceLists.ToListAsync());
        }

        // GET: InvoiceLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceList = await _context.InvoiceLists
                .FirstOrDefaultAsync(m => m.InvoiceId == id);
            if (invoiceList == null)
            {
                return NotFound();
            }

            return View(invoiceList);
        }

        // GET: InvoiceLists/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InvoiceLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InvoiceId,InvoiceNum,BillTo,Date,DueDate,Status,Amount")] InvoiceList invoiceList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(invoiceList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(invoiceList);
        }

        // GET: InvoiceLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceList = await _context.InvoiceLists.FindAsync(id);
            if (invoiceList == null)
            {
                return NotFound();
            }
            return View(invoiceList);
        }

        // POST: InvoiceLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InvoiceId,InvoiceNum,BillTo,Date,DueDate,Status,Amount")] InvoiceList invoiceList)
        {
            if (id != invoiceList.InvoiceId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(invoiceList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InvoiceListExists(invoiceList.InvoiceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(invoiceList);
        }

        // GET: InvoiceLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceList = await _context.InvoiceLists
                .FirstOrDefaultAsync(m => m.InvoiceId == id);
            if (invoiceList == null)
            {
                return NotFound();
            }

            return View(invoiceList);
        }

        // POST: InvoiceLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var invoiceList = await _context.InvoiceLists.FindAsync(id);
            _context.InvoiceLists.Remove(invoiceList);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InvoiceListExists(int id)
        {
            return _context.InvoiceLists.Any(e => e.InvoiceId == id);
        }
    }
}
