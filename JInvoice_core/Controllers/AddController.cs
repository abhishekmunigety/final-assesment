﻿using JInvoice_core.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace JInvoice_core.Controllers
{
    public class AddController : Controller
    {
        Invoice_ProductContext db = new Invoice_ProductContext();

        public IActionResult Index()
        {
          
           

            return View();
        }

        public ActionResult CreateForm()
        {
            var list = new List<string>() { "mahesh", "ramesh", "suresh" };
            ViewBag.list = list;
            var list1 = new List<string>() { "Paid", "sent", "Draft" };
            ViewBag.list1 = list1;
            var list2 = new List<string>() { "watch", "pen", "book" ,"bottle"};
            ViewBag.list2 = list2;
            return View();
 }
        [HttpPost]
        public ActionResult SaveForm(ViewModel v)
        {
            if(ModelState.IsValid==true)
            {
                db.InvoiceLists.Add(v.model1);
                db.InvoiceDetails.Add(v.model2);
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["InsertMsg"] = "<script>alert('Inserted)</script>";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["InsertMsg"] = "<script>alert('Inserted)</script>";
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
    }
}
