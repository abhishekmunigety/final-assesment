﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace JInvoice_core.Models
{
    public partial class Invoice_ProductContext : DbContext
    {
        public Invoice_ProductContext()
        {
        }

        public Invoice_ProductContext(DbContextOptions<Invoice_ProductContext> options)
            : base(options)
        {
        }

        public virtual DbSet<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual DbSet<InvoiceList> InvoiceLists { get; set; }
        public object ViewModel { get; internal set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("server=T066\\SQLEXPRESS;database =Invoice_Product;trusted_connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<InvoiceDetail>(entity =>
            {
                entity.HasKey(e => e.ProductId);

                entity.ToTable("Invoice_Details");

                entity.Property(e => e.ProductId).HasColumnName("Product_ID");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.InvoiceId).HasColumnName("Invoice_ID");

                entity.Property(e => e.Product)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("product");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.InvoiceDetails)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Invoice_Details_Invoice_List");
            });

            modelBuilder.Entity<InvoiceList>(entity =>
            {
                entity.HasKey(e => e.InvoiceId);

                entity.ToTable("Invoice_List");

                entity.Property(e => e.InvoiceId).HasColumnName("Invoice_ID");

                entity.Property(e => e.BillTo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Bill_To");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.DueDate)
                    .HasColumnType("date")
                    .HasColumnName("Due_Date");

                entity.Property(e => e.InvoiceNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
