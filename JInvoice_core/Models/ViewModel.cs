﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JInvoice_core.Models
{
    public class ViewModel
    {
        [Key]
        public InvoiceList model1 { get; set; }

        public InvoiceDetail model2 { get; set; }
    }
}
