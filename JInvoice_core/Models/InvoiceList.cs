﻿using System;
using System.Collections.Generic;
using PagedList;

#nullable disable

namespace JInvoice_core.Models
{
    public partial class InvoiceList
    {
        public InvoiceList()
        {
            InvoiceDetails = new HashSet<InvoiceDetail>();
        }

        public int InvoiceId { get; set; }
        public string InvoiceNum { get; set; }
        public string BillTo { get; set; }
        public DateTime Date { get; set; }
        public DateTime DueDate { get; set; }
        public string Status { get; set; }
        public int Amount { get; set; }

        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
    }
}
