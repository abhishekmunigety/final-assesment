﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JInvoice_core.Models
{
    public partial class InvoiceDetail
    {
        public int ProductId { get; set; }
        public string Product { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public int Tax { get; set; }
        public int Total { get; set; }
        public int InvoiceId { get; set; }

        public virtual InvoiceList Invoice { get; set; }
    }
}
