#pragma checksum "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d6206cdd4f9b3d52ace0f2e0a25f783d627e8e29"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_InvoiceDetails_Create), @"mvc.1.0.view", @"/Views/InvoiceDetails/Create.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\_ViewImports.cshtml"
using JInvoice_core;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\_ViewImports.cshtml"
using JInvoice_core.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d6206cdd4f9b3d52ace0f2e0a25f783d627e8e29", @"/Views/InvoiceDetails/Create.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2d575b2f255d1b086e9fdd06103c4b4061f2ede4", @"/Views/_ViewImports.cshtml")]
    public class Views_InvoiceDetails_Create : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<JInvoice_core.Models.InvoiceDetail>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/jquery.validate.unobtrusive.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/jquery-3.4.1.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
  
    ViewData["Title"] = "Create";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Create</h1>\r\n\r\n<h4>InvoiceDetail</h4>\r\n<hr />\r\n<div class=\"row\">\r\n    <div class=\"col-md-4\">\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6206cdd4f9b3d52ace0f2e0a25f783d627e8e295360", async() => {
                WriteLiteral(@"
            <div>
                <table class=""table table-bordered"">
                    <thead>



                        <tr>
                            <th>PRODUCTID</th>
                            <th>Product</th>
                            <th>Description</th>
                            <th>Price(INR)</th>
                            <th>Quantity</th>
                            <th>Tax(%)</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>



                    </thead>



                    <tbody id=""data_table"">
                        <tr>
                            <td>




                                ");
#nullable restore
#line 44 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.EditorFor(model => model.ProductId, new { htmlAttributes = new { @id = "auto_id", @class = "form-control" } }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                ");
#nullable restore
#line 45 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.ValidationMessageFor(model => model.ProductId, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n\r\n\r\n                            </td>\r\n\r\n\r\n\r\n                            <td>\r\n\r\n\r\n\r\n\r\n                                ");
#nullable restore
#line 58 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.EditorFor(model => model.Product, new { htmlAttributes = new { @id = "product", @class = "form-control" } }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n\r\n\r\n\r\n                                ");
#nullable restore
#line 63 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Product, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                            </td>\r\n\r\n\r\n\r\n\r\n                            <td>\r\n\r\n\r\n                                ");
#nullable restore
#line 72 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.EditorFor(model => model.Description, new { htmlAttributes = new { @id = "description", @class = "form-control" } }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                ");
#nullable restore
#line 73 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Description, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n\r\n\r\n                            </td>\r\n\r\n\r\n\r\n                            <td>\r\n\r\n\r\n\r\n                                ");
#nullable restore
#line 85 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.EditorFor(model => model.Price, new { htmlAttributes = new { @id = "price", @class = "form-control" } }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                ");
#nullable restore
#line 86 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Price, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n\r\n\r\n                            </td>\r\n\r\n\r\n\r\n                            <td>\r\n\r\n\r\n\r\n\r\n                                ");
#nullable restore
#line 99 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.EditorFor(model => model.Quantity, new { htmlAttributes = new { @name = "tax1", @id = "qty", @class = "form-control" } }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                ");
#nullable restore
#line 100 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Quantity, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n\r\n\r\n                            </td>\r\n\r\n\r\n\r\n                            <td>\r\n\r\n\r\n\r\n                                ");
#nullable restore
#line 112 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.EditorFor(model => model.Tax, new { htmlAttributes = new { @name = "tax1", @id = "tax", @class = "form-control" } }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                ");
#nullable restore
#line 113 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Tax, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n\r\n\r\n                            </td>\r\n\r\n\r\n                            <td>\r\n\r\n\r\n\r\n\r\n                                ");
#nullable restore
#line 125 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.EditorFor(model => model.Total, new { htmlAttributes = new { @name = "total1", @id = "total", @class = "form-control" } }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                ");
#nullable restore
#line 126 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Total, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"



                            </td>




                            <td>
                                <input type=""button"" class=""add btn btn-success""
                                       onclick=""add_row()"" value=""Add New"">



                            </td>

                            <td>
                                <input type=""button"" class=""add btn btn-danger""
                                       onclick=""delete_row(1)"" value=""Delete"">
                            </td>

                        </tr>



                        <tr>
                            <td></td>
                            <td>Sub Total :</td>
                            <td><span id=""sub_total"">0</span></td>
                        </tr>


                        <tr>
                            <td></td>
                            <td>Total Tax :</td>
                            <td><span id=""total_tax"">0</span></td>
                        </tr>



                      ");
                WriteLiteral("  <tr>\r\n                            <td></td>\r\n                            <td>Grand Total:</td>\r\n                            <td><span id=\"grand_total\">0</span></td>\r\n                        </tr>\r\n\r\n\r\n\r\n\r\n\r\n");
                WriteLiteral(@"


                </table>
                <div class=""form-group col-md-12"">
                    <label>Attachment(Optional) :</label>
                </div>
                <div class=""form-group col-md-4"">
                    <input type=""file"" class=""form-control"" id=""attachment""
                           placeholder=""Attachment"">
                </div>




            </div>

        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n\r\n    </div>\r\n</div>\r\n\r\n<div class=\"form-group\">\r\n    <input type=\"submit\" value=\"save\" class=\"btn btn-primary\" />\r\n</div>\r\n\r\n<div>\r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6206cdd4f9b3d52ace0f2e0a25f783d627e8e2915614", async() => {
                WriteLiteral("Back to List");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n");
#nullable restore
#line 212 "C:\Users\abhishek.munigety\source\repos\JInvoice_core\JInvoice_core\Views\InvoiceDetails\Create.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
#nullable disable
            }
            );
            WriteLiteral(@"
<script type=""text/javascript"">
    function add_row1() { console.log(""sadas"") }
    function add_row() {
        var auto_id = document.getElementById(""auto_id"").value;
        var product = document.getElementById(""product"").value;
        var description = document.getElementById(""description"").value;
        var price = document.getElementById(""price"").value;
        var qty = document.getElementById(""qty"").value;
        var tax = document.getElementById(""tax"").value;
        var total = parseInt(price) * parseInt(qty);



        if (product == """") {
            alert(""product must be filled out"");
        } else if (description == """") {
            alert(""description must be filled out"");
        } else if (price == """") {
            alert(""price must be filled out"");
        } else if (qty == """") {
            alert(""qty must be filled out"");
        } else {
            var table = document.getElementById(""data_table"");
            var table_len = (table.rows.length) - 3;
  ");
            WriteLiteral(@"          console.log(table_len);
            //""<td id='auto_id"" + table_len + ""'><input type='text' value='"" + auto_id + ""' class='form-control'></td> "" +
            var row = table.insertRow(table_len).outerHTML = ""<tr id='row"" + table_len + ""'> ""
                + ""<td id='auto_id"" + table_len + ""'><input type='text' value='""
                + auto_id
                + ""' class='form-control'></td> ""
                + ""<td id='product"" + table_len + ""'>""
                + product
                + ""</td> ""
                + ""<td id='description"" + table_len + ""'>""
                + description
                + ""</td> ""
                + ""<td id='price"" + table_len + ""'>""
                + price
                + ""</td> ""
                + ""<td id='qty"" + table_len + ""'>""
                + qty
                + ""</td> ""
                + ""<td id='tax"" + table_len + ""'>""
                + tax
                + ""</td> ""
                + ""<td id='total"" + table_len + ""'>""
            ");
            WriteLiteral(@"    + total
                + ""</td> ""
                +



                ""<td><i class='fa fa-trash' style='font-size:24px' onclick='delete_row(""
                + table_len + "")'> </i></td> "" + ""</tr>"";




            var sub_total = 0;
            var total_tax = 0;
            for (var i = 1; i <= table_len; i++) {
                sub_total += parseInt(document.getElementById(""total"" + i).innerText);
                total_tax += parseInt(document.getElementById(""tax"" + i).innerText);
            }



            document.getElementById(""auto_id"").value = parseInt(auto_id) + 1;
            document.getElementById(""product"").value;
            document.getElementById(""description"").value = """";
            document.getElementById(""price"").value = """";
            document.getElementById(""qty"").value = """";
            document.getElementById(""tax"").value = """";
            document.getElementById(""total"").value = """";
            document.getElementById(""sub_total"").innerHTML = sub");
            WriteLiteral(@"_total;
            document.getElementById(""total_tax"").innerHTML = total_tax;
            document.getElementById(""grand_total"").innerHTML = sub_total + total_tax;
        }

    }
    //function delete_row(no) {
    // document.getElementById(""row"" + no + """").outerHTML = """";
    // //document.getElementById(""sub_total"").innerHTML = """";
    // //document.getElementById(""total_tax"").innerHTML = """";
    // //document.getElementById(""grand_total"").innerHTML = """";
    //}
    function delete_row(no) {

        var table = document.getElementById(""data_table"");
        var table_len = (table.rows.length) - 3;
        var sub_total = document.getElementById(""sub_total"").innerHTML;
        var total_tax = document.getElementById(""total_tax"").innerHTML;
        var grand_total = document.getElementById(""grand_total"").innerHTML;
        console.log(sub_total + "":"" + total_tax + "":"" + grand_total);
        for (var i = 1; i <= table_len; i++) {
            if (no == i) {
                var part");
            WriteLiteral(@"icular_tot = parseFloat(document.getElementById(""total"" + i).innerHTML);
                var particular_tax = parseFloat(document.getElementById(""tax"" + i).innerHTML)
                console.log(particular_tot + "":"" + particular_tax);



                var after_deletion_tot = parseFloat(sub_total) - particular_tot;
                var after_deletion_tax = parseFloat(total_tax) - particular_tax;
                console.log(after_deletion_tot + "":"" + after_deletion_tax);
                document.getElementById(""sub_total"").innerHTML = after_deletion_tot;
                document.getElementById(""total_tax"").innerHTML = after_deletion_tax;
                document.getElementById(""grand_total"").innerHTML = (after_deletion_tax + after_deletion_tot);
            }
        }
        document.getElementById(""row"" + no + """").outerHTML = """";
    }
</script>
");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6206cdd4f9b3d52ace0f2e0a25f783d627e8e2922473", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6206cdd4f9b3d52ace0f2e0a25f783d627e8e2923513", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n<script>\r\n    $(function () {\r\n        $(\'.datepicker\').datepicker({\r\n            dateFormat: \"yy-mm-dd\",\r\n            changeMonth: true,\r\n            changeYear: true\r\n        });\r\n\r\n\r\n\r\n    });\r\n</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<JInvoice_core.Models.InvoiceDetail> Html { get; private set; }
    }
}
#pragma warning restore 1591
